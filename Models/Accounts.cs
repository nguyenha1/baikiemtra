﻿using System.ComponentModel.DataAnnotations;

namespace BaiKiemTra.Models
{
    public class Accounts
    {
        public int AccountsId { get; set; }
        [Required, StringLength(100)]
        public string CustomerId     { get; set; }
        [Range(0.01, 10000.00)]

        public string Accountname { get; set; }
        public int AddTextHere { get; set; }
    }
}
